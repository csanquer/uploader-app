'use strict';

var config = require('config');
var MongoClient = require('mongodb');
var mongoose = require('mongoose');

var createMongoUri = function(config) {
    var uri = '',
        authPart = '';

    // Get authentication database part
    if (config.hasOwnProperty('authdb') && config.authdb) {
        authPart = `?authSource=${config.authdb}`;
    }

    uri += `mongodb://${config.host}:${config.port}/${config.db}${authPart}`;

    return uri;
};

mongoose.Promise = Promise;
var db = mongoose.createConnection(createMongoUri(config.mongo), {
    user: config.mongo.user, 
    pass: config.mongo.pass,
    useNewUrlParser: true
});

mongoose.set('useCreateIndex', true);

db.on('error', function(error) {
    console.error('MongoDB connection error: ' + error);
});

db.on('connected', function() {
    console.log('connected to MongoDB');
});

module.exports = db;
